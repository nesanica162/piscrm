﻿using Contracts.Dto;
using System;
using System.Web;

namespace Framework
{
    public static class StringExtensions
    {
        public static string ToHtmlContent(this string content)
        {
            var result = string.Empty;

            if (!String.IsNullOrEmpty(content))
            {
                var contentArray = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                foreach (var item in contentArray)
                    if (!string.IsNullOrEmpty(item))
                        result += "<p>" + item + "</p>";
            }

            return result;
        }
        public static string ToTextContent(this string content)
        {
            var ret = string.Empty;

            if (!String.IsNullOrEmpty(content))
            {
                ret = content.Replace("<p>&nbsp;</p>", String.Empty).Replace("<p>", string.Empty).Replace("</p>", Environment.NewLine);
            }

            return ret;
        }   
    }
}