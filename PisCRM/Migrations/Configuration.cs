﻿using Domain.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PisCRM.Contracts;
using PisCRM.Contracts.Enums;
using PisCRM.Contracts.IdentityModels;
using PisCRM.Helpers;
using PisCRM.Repository;
using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using Framework;

namespace PisCRM.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "PisCRM.Data.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //user roles
            var userRoles = Enum.GetValues(typeof(Roles));
            foreach (var role in userRoles)
            {
                int id = Convert.ToInt32(role);
                context.Roles.AddOrUpdate(
                    p => p.Id,
                    new CustomRole()
                    {
                        Id = id,
                        Name = ((Roles)id).GetDescription()
                    });
            }

            string email = "t.curlinovska@gmail.com";
            if (!context.Users.Any(u => u.Email == email))
            {
                UserManager<ApplicationUser, int> userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = new ApplicationUser
                {
                    UserName = email,
                    Email = email,
                    DateCreated = DateTime.UtcNow,
                    FirstName = "Тина",
                    LastName = "Чурлиновска",
                    WorkPosition = "Бизнис аналист"
                };

                userManager.Create(user, "0915139jassumtina");
                userManager.AddToRole(user.Id, (Roles.Admin).GetDescription());
            }

            context.SaveChanges();
        }
    }
}