namespace PisCRM.Infrastructure.Tasks
{
	public interface IRunOnEachRequest
	{
		void Execute();
	}
}