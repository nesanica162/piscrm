namespace PisCRM.Infrastructure.Tasks
{
	public interface IRunAfterEachRequest
	{
		void Execute();
	}
}