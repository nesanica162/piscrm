namespace PisCRM.Infrastructure.Tasks
{
	public interface IRunOnError
	{
		void Execute();
	}
}