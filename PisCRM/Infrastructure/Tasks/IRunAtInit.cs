﻿namespace PisCRM.Infrastructure.Tasks
{
	public interface IRunAtInit
	{
		void Execute();
	}
}