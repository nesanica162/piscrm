namespace PisCRM.Infrastructure.Tasks
{
	public interface IRunAtStartup
	{
		void Execute();
	}
}