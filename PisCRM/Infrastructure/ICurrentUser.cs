﻿using PisCRM.Contracts;

namespace PisCRM.Infrastructure
{
    public interface ICurrentUser
    {
        ApplicationUser User { get; }
    }
}