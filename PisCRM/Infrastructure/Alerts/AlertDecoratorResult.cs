﻿using System.Web.Mvc;

namespace PisCRM.Infrastructure.Alerts
{
	public class AlertDecoratorResult : ActionResult
	{
		public ActionResult InnerResult { get; set; }
		public string AlertClass { get; set; }
		public string Message { get; set; }
        public string Icon { get; set; }

		public AlertDecoratorResult(ActionResult innerResult, string alertClass, string message, string icon)
		{
			InnerResult = innerResult;
			AlertClass = alertClass;
			Message = message;
            Icon = icon;
		}

		public override void ExecuteResult(ControllerContext context)
		{
			var alerts = context.Controller.TempData.GetAlerts();
			alerts.Add(new Alert(AlertClass, Message, Icon));
			InnerResult.ExecuteResult(context);
		}
	}
}