﻿namespace PisCRM.Infrastructure.Alerts
{
	public class Alert
	{
		public string AlertClass { get; set; }
		public string Message { get; set; }
        public string Icon { get; set; }

		public Alert(string alertClass, string message, string icon)
		{
			AlertClass = alertClass;
			Message = message;
            Icon = icon;
		}
	}
}