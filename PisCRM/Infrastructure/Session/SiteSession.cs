﻿using PisCRM.Contracts;

namespace PisCRM.Infrastructure.Session
{
    /// <summary>
    /// Defines the site session.
    /// </summary>
    /// <remarks>
    /// Used to cache only the needed data for the current user!
    /// </remarks>
    public class SiteSession
    {
        public static ApplicationUser CurrentUser { get; set; }

        /// <summary>
        /// Initializes a new instance of the SiteSession class.
        /// </summary>
        /// <param name="db">The data context.</param>
        /// <param name="user">The current user.</param>
        public SiteSession()
        {
        }
    }
}