﻿using System.Security.Principal;
using Microsoft.AspNet.Identity;
using PisCRM.Caching;
using PisCRM.Repository;
using PisCRM.Contracts;

namespace PisCRM.Infrastructure
{
    public class CurrentUser : ICurrentUser
    {
        private readonly IIdentity _identity;
        private readonly ApplicationDbContext _context;
        private readonly ICacheManager _cacheManager;

        private ApplicationUser _user;

        public CurrentUser(IIdentity identity, ApplicationDbContext context, ICacheManager cacheManager)
        {
            _identity = identity;
            _context = context;
            _cacheManager = cacheManager;
        }

        public ApplicationUser User
        {
            get
            {
                return _user ?? (_user = _context.Users.Find(_identity.GetUserId<int>()));
            }
        }
    }
}