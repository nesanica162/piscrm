﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PisCRM.Infrastructure.Mapping;
using System.ComponentModel;
using PisCRM.Repository;
using PisCRM.Contracts;

namespace PisCRM.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Внесете Email адреса.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Невалидна Email адреса.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Внесете Лозинка.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Задржи ме најавен")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Внесете Email адреса.")]
        [EmailAddress(ErrorMessage = "Внесете валидна Email адреса.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Внесете Лозинка.")]
        [DataType(DataType.Password)]
        [Display(Name = "Лозинка")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потврди лозинка")]
        [Compare("Password", ErrorMessage = "Лозинките не се совпаѓаат.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Внесете Име.")]
        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Име")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Внесете Презиме.")]
        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Презиме")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Селектирајтe Роља")]
        [Display(Name = "Селектирајтe Роља")]
        public int RoleId { get; set; }

        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Работна позиција")]
        public string WorkPosition { get; set; }
    }

    public class PersonalInfoViewModel : IMapFrom<ApplicationUser>
    {
        [System.Web.Mvc.HiddenInput]
        public int Id { get; set; }

        //[Required(ErrorMessage = "Внесете Email адреса.")]
        //[EmailAddress]
        //[Display(Name = "Email")]
        //public string Email { get; set; }

        [Required(ErrorMessage = "Внесете име.")]
        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Име")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Внесете презиме.")]
        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Презиме")]
        public string LastName { get; set; }

        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Работна позиција")]
        public string WorkPosition { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Внесете Email адреса.")]
        [EmailAddress(ErrorMessage = "Невалидна Email адреса.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Внесете лозинка.")]
        [StringLength(100, ErrorMessage = "Лозинката мора да биде најмалку {2} карактери.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Лозинка")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потврди лозинка")]
        [Compare("Password", ErrorMessage = "Лозинките не се совпаѓаат.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Внесете Email адреса.")]
        [EmailAddress(ErrorMessage = "Невалидна Email адреса.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
