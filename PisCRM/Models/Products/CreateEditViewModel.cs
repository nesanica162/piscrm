﻿using System.ComponentModel.DataAnnotations;

namespace PisCRM.Models.Products
{
    public class CreateEditViewModel
    {
        [System.Web.Mvc.HiddenInput]
        public int Id { get; set; }

        [Required(ErrorMessage = "Внесете Име")]
        [StringLength(200, ErrorMessage = "Max. 200 карактери.")]
        [Display(Name = "Име")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Внесете Опис.")]
        [StringLength(1000, ErrorMessage = "Max. 1000 карактери.")]
        [Display(Name = "Опис")]
        public string Description { get; set; }
    }
}