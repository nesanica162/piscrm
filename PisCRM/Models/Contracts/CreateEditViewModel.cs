﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PisCRM.Models.Contracts
{
    public class CreateEditViewModel
    {
        [System.Web.Mvc.HiddenInput]
        public int Id { get; set; }

        [Required(ErrorMessage = "Внесете Име")]
        [StringLength(200, ErrorMessage = "Max. 200 карактери.")]
        [Display(Name = "Име")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Внесете Опис.")]
        [StringLength(1000, ErrorMessage = "Max. 1000 карактери.")]
        [Display(Name = "Опис")]
        public string Description { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Почетен датум")]
        public DateTime StartDate { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Краен датум")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "Селектирајтe Клиент.")]
        [Display(Name = "Селектирајте Клиент")]
        public int ClientId { get; set; }
    }
}