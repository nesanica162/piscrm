﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PisCRM.Models.Employees
{
    public class EditViewModel
    {
        [System.Web.Mvc.HiddenInput]
        public int Id { get; set; }

        //[Required(ErrorMessage = "Внесете Email адреса.")]
        //[EmailAddress(ErrorMessage = "Внесете валидна Email адреса.")]
        //[Display(Name = "Email")]
        //public string Email { get; set; }

        [Required(ErrorMessage = "Внесете Име.")]
        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Име")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Внесете Презиме.")]
        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Презиме")]
        public string LastName { get; set; }

        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Работна позиција")]
        public string WorkPosition { get; set; }

        [Required(ErrorMessage = "Селектирајтe Роља")]
        [Display(Name = "Селектирајтe Роља")]
        public int RoleId { get; set; }

        [Display(Name = "Профилна слика")]
        public string ProfilePictureUrl { get; set; }
    }
}