﻿using System.ComponentModel.DataAnnotations;

namespace PisCRM.Models.Clients
{
    public class CreateEditViewModel
    {
        [System.Web.Mvc.HiddenInput]
        public int Id { get; set; }

        [Required(ErrorMessage = "Внесете Име и презиме")]
        [StringLength(200, ErrorMessage = "Max. 200 карактери.")]
        [Display(Name = "Име и презиме")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Внесете Email адреса.")]
        [EmailAddress(ErrorMessage = "Внесете валидна Email адреса.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Внесете Компанија.")]
        [StringLength(100, ErrorMessage = "Max. 100 карактери.")]
        [Display(Name = "Компанија")]
        public string Company { get; set; }

        [StringLength(1000, ErrorMessage = "Max. 1000 карактери.")]
        [Display(Name = "Проекти")]
        public string Projects { get; set; }
    }
}