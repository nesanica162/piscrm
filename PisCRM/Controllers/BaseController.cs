﻿using PisCRM.Infrastructure;
using Microsoft.Web.Mvc;
using PisCRM.Infrastructure.Session;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using PisCRM.Repository;

namespace PisCRM.Controllers
{
    /// <summary>
    /// Defines the base controller.
    /// </summary>
    /// <remarks>
    /// This is the base class for all site's controllers.
    /// </remarks>
    public abstract class BaseController : Controller
    {
        private readonly ICurrentUser _currentUser;

        public BaseController(ICurrentUser currentUser)
        {
            _currentUser = currentUser;
        }

        protected override bool DisableAsyncSupport
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the current site session.
        /// </summary>
        public SiteSession CurrentSiteSession
        {
            get
            {
                SiteSession shopSession = (SiteSession)this.Session["SiteSession"];
                return shopSession;
            }
        }

        public void EnableLazyLoadingContext(ApplicationDbContext context)
        {
            context.Configuration.ProxyCreationEnabled = true;
            context.Configuration.LazyLoadingEnabled = true;
        }

        /// <summary>
        /// The data context.
        /// </summary>
        //protected MvcBasicSiteEntities _db = new MvcBasicSiteEntities();

        /// <summary>
        /// Dispose the used resource.
        /// </summary>
        /// <param name="disposing">The disposing flag.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    _db.Dispose();
        //    base.Dispose(disposing);
        //}

        /// <summary>
        /// Manage the internationalization before to invokes the action in the current controller context.
        /// </summary>
        protected override void ExecuteCore()
        {
            ViewBag.CurrentController = ControllerContext.RouteData.Values["Controller"];
            ViewBag.CurrentAction = ControllerContext.RouteData.Values["Action"];

            SiteSession.CurrentUser = _currentUser.User;
            //
            // Invokes the action in the current controller context.
            //
            base.ExecuteCore();
        }

        /// <summary>
        /// Called when an unhandled exception occurs in the action.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is UnauthorizedAccessException)
            {
                filterContext.ExceptionHandled = true;
                filterContext.Result = RedirectToAction("Home", "Index");
            }
            //
            base.OnException(filterContext);
        }
        protected ActionResult RedirectToAction<TController>(Expression<Action<TController>> action)
            where TController : Controller
        {
            return ControllerExtensions.RedirectToAction(this, action);
        }
    }
}