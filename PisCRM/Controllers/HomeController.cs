﻿using PisCRM.Infrastructure;
using PisCRM.Repository;
using System.Web.Mvc;

namespace PisCRM.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly ICurrentUser _currentUser;

        public HomeController(ApplicationDbContext context, ICurrentUser currentUser)
            : base(currentUser)
        {
            _context = context;
            _currentUser = currentUser;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}