﻿using PisCRM.Framework;
using PisCRM.Infrastructure;
using System;
using System.Linq;
using System.Web.Mvc;
using PisCRM.Infrastructure.Alerts;
using PisCRM.Models.Employees;
using Domain.Managers;
using PisCRM.Contracts;
using Omu.ValueInjecter;
using PisCRM.Models.Clients;

namespace PisCRM.Controllers
{
    [Authorize]
    public class ClientsController : BaseController
    {
        private readonly ClientManager _clientManager;

        public ClientsController(ICurrentUser currentUser, ClientManager clientManager) : base(currentUser)
        {
            _clientManager = clientManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexPaging(int page = 1, string searchWord = "")
        {
            var model = new PagedData<Client>();
            IQueryable<Client> users = _clientManager.GetClients(searchWord);
            model.Data = model.PreparePage(users);

            return View(model);
        }

        //[AdminRoleFilter]
        public ActionResult Create()
        {
            var model = new CreateEditViewModel();
            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Create(CreateEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var client = new Client { DateCreated = DateTime.UtcNow };
            client.InjectFrom(model);

            _clientManager.Insert(client);

            //if (Request.Files.Count == 1)
            //_employeeManager.SaveProfilePhoto(user.Id, Request.Files[0]);

            return RedirectToAction<ClientsController>(c => c.Index())
                .WithSuccess("Клиентот е успешно креиран!");
        }

        public ActionResult Details(int id)
        {
            var model = _clientManager.GetSingle(id);

            if (model == null)
            {
                return RedirectToAction<ClientsController>(c => c.Index())
                    .WithError("Не постои таков клиент.");
            }

            return View(model);
        }

        //[AdminRoleFilter]
        public ActionResult Edit(int id)
        {
            var user = _clientManager.GetSingle(id);

            if (user == null)
            {
                return RedirectToAction<ClientsController>(c => c.Index())
                    .WithError("Не е пронајден клиентот! Обидете се повторно.");
            }

            var model = new EditViewModel();
            model.InjectFrom(user);
            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Edit(CreateEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var client = _clientManager.GetSingle(model.Id);

            if (client == null)
                return RedirectToAction<ClientsController>(c => c.Index())
                    .WithError("Не е пронајден клиентот! Обидете се повторно.");

            client.Name = model.Name;
            client.Email = model.Email;
            client.Company = model.Company;
            client.Projects = model.Projects;

            //if (Request.Files.Count == 1)
            //    _employeeManager.SaveProfilePhoto(user.Id, Request.Files[0]);

            _clientManager.Update(client);

            return RedirectToAction<ClientsController>(c => c.Index())
                .WithSuccess("Информациите за клиентот се успешно зачувани!");
        }

        //[AdminRoleFilter]
        public ActionResult Delete(int id)
        {
            var model = _clientManager.GetSingle(id);

            if (model == null)
                return RedirectToAction<ClientsController>(c => c.Index())
                    .WithError("Не е пронајден клиентот! Обидете се повторно.");

            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var client = _clientManager.GetSingle(id);

            if (client == null)
                return RedirectToAction<ClientsController>(c => c.Index())
                    .WithError("Не е пронајден клиентот! Обидете се повторно.");

            _clientManager.Delete(client.Id);

            return RedirectToAction<ClientsController>(c => c.Index())
                .WithSuccess("Клиентот е успешно избришан!");
        }
    }
}