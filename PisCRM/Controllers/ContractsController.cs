﻿using PisCRM.Framework;
using PisCRM.Infrastructure;
using System;
using System.Linq;
using System.Web.Mvc;
using PisCRM.Infrastructure.Alerts;
using Domain.Managers;
using PisCRM.Contracts;
using Omu.ValueInjecter;
using PisCRM.Models.Contracts;

namespace PisCRM.Controllers
{
    [Authorize]
    public class ContractsController : BaseController
    {
        private readonly ContractManager _contractManager;

        public ContractsController(ICurrentUser currentUser, ContractManager contractManager)
            : base(currentUser)
        {
            _contractManager = contractManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexPaging(int page = 1, string searchWord = "")
        {
            var model = new PagedData<Contract>();
            IQueryable<Contract> users = _contractManager.GetContracts(searchWord);
            model.Data = model.PreparePage(users);

            return View(model);
        }

        //[AdminRoleFilter]
        public ActionResult Create()
        {
            var model = new CreateEditViewModel();
            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Create(CreateEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var contract = new Contract { DateCreated = DateTime.UtcNow };
            contract.InjectFrom(model);

            _contractManager.Insert(contract);

            return RedirectToAction<ContractsController>(c => c.Index())
                .WithSuccess("Договорот е успешно креиран!");
        }

        public ActionResult Details(int id)
        {
            var model = _contractManager.GetSingle(id);

            if (model == null)
            {
                return RedirectToAction<ContractsController>(c => c.Index())
                    .WithError("Не постои таков договор.");
            }

            return View(model);
        }

        //[AdminRoleFilter]
        public ActionResult Edit(int id)
        {
            var contract = _contractManager.GetSingle(id);

            if (contract == null)
            {
                return RedirectToAction<ContractsController>(c => c.Index())
                    .WithError("Не е пронајден договорот! Обидете се повторно.");
            }

            var model = new CreateEditViewModel();
            model.InjectFrom(contract);
            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Edit(CreateEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var contract = _contractManager.GetSingle(model.Id);

            if (contract == null)
                return RedirectToAction<ContractsController>(c => c.Index())
                    .WithError("Не е пронајден договорот! Обидете се повторно.");

            contract.InjectFrom(model);

            _contractManager.Update(contract);

            return RedirectToAction<ContractsController>(c => c.Index())
                .WithSuccess("Информациите за договорот се успешно зачувани!");
        }

        //[AdminRoleFilter]
        public ActionResult Delete(int id)
        {
            var model = _contractManager.GetSingle(id);

            if (model == null)
                return RedirectToAction<ContractsController>(c => c.Index())
                    .WithError("Не е пронајден договорoт! Обидете се повторно.");

            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var contract = _contractManager.GetSingle(id);

            if (contract == null)
                return RedirectToAction<ContractsController>(c => c.Index())
                    .WithError("Не е пронајден договорот! Обидете се повторно.");

            _contractManager.Delete(contract.Id);

            return RedirectToAction<ContractsController>(c => c.Index())
                .WithSuccess("Договорот е успешно избришан!");
        }
    }
}