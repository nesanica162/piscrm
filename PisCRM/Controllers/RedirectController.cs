﻿using PisCRM.Infrastructure;
using PisCRM.Repository;
using System.Web.Mvc;

namespace PisCRM.Controllers
{
    public class RedirectController : BaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly ICurrentUser _currentUser;

        public RedirectController(ApplicationDbContext context, ICurrentUser currentUser)
            : base(currentUser)
        {
            _context = context;
            _currentUser = currentUser;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}