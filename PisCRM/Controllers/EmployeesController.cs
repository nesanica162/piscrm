﻿using PisCRM.FIlters;
using PisCRM.Framework;
using PisCRM.Infrastructure;
using PisCRM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PisCRM.Infrastructure.Alerts;
using PisCRM.Models.Employees;
using Domain.Managers;
using PisCRM.Contracts;
using PisCRM.Contracts.Enums;
using Framework;
using System.Security.Principal;
using Omu.ValueInjecter;

namespace PisCRM.Controllers
{
    [Authorize]
    public class EmployeesController : BaseController
    {
        private readonly ApplicationUserManager _userManager;
        private readonly EmployeeManager _employeeManager;
        private readonly RoleManager _roleManager;
        private readonly IIdentity _identity;

        public EmployeesController(ICurrentUser currentUser, ApplicationUserManager userManager,
            EmployeeManager employeeManager, RoleManager roleManager, IIdentity identity)
            : base(currentUser)
        {
            _userManager = userManager;
            _employeeManager = employeeManager;
            _roleManager = roleManager;
            _identity = identity;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexPaging(int page = 1, string searchWord = "")
        {
            var model = new PagedData<ApplicationUser>();
            IQueryable<ApplicationUser> users = _employeeManager.GetEmployees(searchWord);
            model.Data = model.PreparePage(users);

            return View(model);
        }

        [AdminRoleFilter]
        public ActionResult Create()
        {
            var model = new RegisterViewModel();
            model.RoleId = (int)Roles.Admin;

            return View(model);
        }

        [AdminRoleFilter]
        [HttpPost]
        public ActionResult Create(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = new ApplicationUser { UserName = model.Email, DateCreated = DateTime.UtcNow };
            user.InjectFrom(model);

            var result = _userManager.Create(user, model.Password);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("CreateUserErrorResponse", result.Errors.First());
                return View(model);
            }
            else
            {
                var role = Enum.GetName(typeof(Roles), model.RoleId);

                if (role == null)
                {
                    AddErrors(result);
                    ModelState.AddModelError("CreateUserErrorResponse", result.Errors.First());
                    return View(model);
                }
                else
                {
                    var roleResult = _userManager.AddToRole(user.Id, ((Roles)model.RoleId).GetDescription());

                    if (!roleResult.Succeeded)
                    {
                        AddErrors(roleResult);
                        ModelState.AddModelError("CreateUserErrorResponse", roleResult.Errors.First());
                        return View(model);
                    }
                }

                if (Request.Files.Count == 1)
                    _employeeManager.SaveProfilePhoto(user.Id, Request.Files[0]);
            }

            return RedirectToAction<EmployeesController>(c => c.Index())
                .WithSuccess("Вработениот е успешно креиран!");
        }

        public ActionResult Details(int id)
        {
            var model = _employeeManager.GetSingle(id);

            if (model == null)
            {
                return RedirectToAction<EmployeesController>(c => c.Index())
                    .WithError("Не постои таков вработен.");
            }

            return View(model);
        }

        [AdminRoleFilter]
        public ActionResult Edit(int id)
        {
            var user = _employeeManager.GetSingle(id);

            if (user == null)
            {
                return RedirectToAction<EmployeesController>(c => c.Index())
                    .WithError("Не е пронајден вработениот! Обидете се повторно.");
            }

            var model = new EditViewModel();
            model.InjectFrom(user);

            if (user.Role != null)
                model.RoleId = user.Role.RoleId;

            return View(model);
        }

        [AdminRoleFilter]
        [HttpPost]
        public ActionResult Edit(EditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = _employeeManager.GetSingle(model.Id);

            if (user == null)
            {
                return RedirectToAction<EmployeesController>(c => c.Index())
                    .WithError("Не е пронајден вработениот! Обидете се повторно.");
            }

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.WorkPosition = model.WorkPosition;

            var roleResult = _employeeManager.SaveEmployeeRole(model.Id, model.RoleId);

            if (!roleResult)
                return RedirectToAction<EmployeesController>(c => c.Index()).WithError("Настана грешка. Ве молиме обидете се повторно.");

            if (Request.Files.Count == 1)
                _employeeManager.SaveProfilePhoto(user.Id, Request.Files[0]);

            _employeeManager.Update(user);

            return RedirectToAction<EmployeesController>(c => c.Index())
                .WithSuccess("Информациите за вработениот се успешно зачувани!");
        }

        [AdminRoleFilter]
        public ActionResult Delete(int id)
        {
            var model = _employeeManager.GetSingle(id);

            if (model == null)
                return RedirectToAction<EmployeesController>(c => c.Index())
                    .WithError("Не е пронајден вработениот! Обидете се повторно.");

            return View(model);
        }

        [AdminRoleFilter]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var user = _employeeManager.GetSingle(id);

            if (user == null)
                return RedirectToAction<EmployeesController>(c => c.Index())
                    .WithError("Не е пронајден вработениот! Обидете се повторно.");

            if (user.Id == _identity.GetUserId<int>())
                return RedirectToAction<EmployeesController>(c => c.Index())
                    .WithError("Не можете да ја избришете сопствената корисничка сметка!");

            _employeeManager.Delete(user.Id);

            return RedirectToAction<EmployeesController>(c => c.Index())
                .WithSuccess("Вработениот е успешно избришан!");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}