﻿using PisCRM.Framework;
using PisCRM.Infrastructure;
using System;
using System.Linq;
using System.Web.Mvc;
using PisCRM.Infrastructure.Alerts;
using Domain.Managers;
using PisCRM.Contracts;
using Omu.ValueInjecter;
using PisCRM.Models.Products;

namespace PisCRM.Controllers
{
    [Authorize]
    public class ProductsController : BaseController
    {
        private readonly ProductManager _productManager;

        public ProductsController(ICurrentUser currentUser, ProductManager productManager) : base(currentUser)
        {
            _productManager = productManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexPaging(int page = 1, string searchWord = "")
        {
            var model = new PagedData<Product>();
            IQueryable<Product> products = _productManager.GetProducts(searchWord);
            model.Data = model.PreparePage(products);

            return View(model);
        }

        //[AdminRoleFilter]
        public ActionResult Create()
        {
            var model = new CreateEditViewModel();
            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Create(CreateEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var product = new Product { DateCreated = DateTime.UtcNow };
            product.InjectFrom(model);

            _productManager.Insert(product);

            //if (Request.Files.Count == 1)
            //_employeeManager.SaveProfilePhoto(user.Id, Request.Files[0]);

            return RedirectToAction<ProductsController>(c => c.Index())
                .WithSuccess("Производот е успешно креиран!");
        }

        public ActionResult Details(int id)
        {
            var model = _productManager.GetSingle(id);

            if (model == null)
            {
                return RedirectToAction<ProductsController>(c => c.Index())
                    .WithError("Не постои таков производ.");
            }

            return View(model);
        }

        //[AdminRoleFilter]
        public ActionResult Edit(int id)
        {
            var product = _productManager.GetSingle(id);

            if (product == null)
            {
                return RedirectToAction<ProductsController>(c => c.Index())
                    .WithError("Не е пронајден производот! Обидете се повторно.");
            }

            var model = new CreateEditViewModel();
            model.InjectFrom(product);
            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Edit(CreateEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var product = _productManager.GetSingle(model.Id);

            if (product == null)
                return RedirectToAction<ProductsController>(c => c.Index())
                    .WithError("Не е пронајден производот! Обидете се повторно.");

            product.InjectFrom(model);

            //if (Request.Files.Count == 1)
            //    _employeeManager.SaveProfilePhoto(user.Id, Request.Files[0]);

            _productManager.Update(product);

            return RedirectToAction<ProductsController>(c => c.Index())
                .WithSuccess("Информациите за производот се успешно зачувани!");
        }

        //[AdminRoleFilter]
        public ActionResult Delete(int id)
        {
            var model = _productManager.GetSingle(id);

            if (model == null)
                return RedirectToAction<ProductsController>(c => c.Index())
                    .WithError("Не е пронајден производот! Обидете се повторно.");

            return View(model);
        }

        //[AdminRoleFilter]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var product = _productManager.GetSingle(id);

            if (product == null)
                return RedirectToAction<ProductsController>(c => c.Index())
                    .WithError("Не е пронајден производот! Обидете се повторно.");

            _productManager.Delete(product.Id);

            return RedirectToAction<ProductsController>(c => c.Index())
                .WithSuccess("Производот е успешно избришан!");
        }
    }
}