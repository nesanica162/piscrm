﻿using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace PisCRM.Helpers
{
    public static class BootstrapHelpers
    {
        public static IHtmlString BootstrapLabelFor<TModel, TProp>(
                this HtmlHelper<TModel> helper,
                Expression<Func<TModel, TProp>> property)
        {
            return helper.LabelFor(property, new
            {
                @class = ""
            });
        }

        public static IHtmlString BootstrapLabel(
                this HtmlHelper helper,
                string propertyName)
        {
            return helper.Label(propertyName, new
            {
                @class = ""
            });
        }

        public static MvcHtmlString CustomValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> propertyName)
        {
            TagBuilder containerDivBuilder = new TagBuilder("div");
            containerDivBuilder.AddCssClass("alert custom-validation alert-danger alert-dismissable valid");

            TagBuilder iconBuilder = new TagBuilder("i");
            iconBuilder.AddCssClass("fa fa-ban");

            TagBuilder btnCloseBuilder = new TagBuilder("button");
            btnCloseBuilder.AddCssClass("close");
            btnCloseBuilder.Attributes.Add("type", "button");
            btnCloseBuilder.Attributes.Add("data-dismiss", "alert");
            btnCloseBuilder.Attributes.Add("aria-hidden", "true");
            btnCloseBuilder.InnerHtml = "&times;";

            string modelName = ExpressionHelper.GetExpressionText(propertyName);
            ModelState state = helper.ViewData.ModelState[modelName];
            if (state != null)
                if ((state.Errors != null) && (state.Errors.Count > 0))
                    containerDivBuilder.AddCssClass("invalid");
                else
                    containerDivBuilder.AddCssClass("valid");

            containerDivBuilder.InnerHtml += iconBuilder.ToString(TagRenderMode.Normal);
            containerDivBuilder.InnerHtml += btnCloseBuilder.ToString(TagRenderMode.Normal);
            containerDivBuilder.InnerHtml += helper.ValidationMessageFor(propertyName).ToString();

            return MvcHtmlString.Create(containerDivBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString CustomValidationMessage(this HtmlHelper helper, string propertyName)
        {
            TagBuilder containerDivBuilder = new TagBuilder("div");
            containerDivBuilder.AddCssClass("alert custom-validation alert-danger alert-dismissable valid");

            TagBuilder iconBuilder = new TagBuilder("i");
            iconBuilder.AddCssClass("fa fa-ban");

            TagBuilder btnCloseBuilder = new TagBuilder("button");
            btnCloseBuilder.AddCssClass("close");
            btnCloseBuilder.Attributes.Add("type", "button");
            btnCloseBuilder.Attributes.Add("data-dismiss", "alert");
            btnCloseBuilder.Attributes.Add("aria-hidden", "true");
            btnCloseBuilder.InnerHtml = "&times;";

            string modelName = ExpressionHelper.GetExpressionText(propertyName);
            ModelState state = helper.ViewData.ModelState[modelName];
            if (state != null)
                if ((state.Errors != null) && (state.Errors.Count > 0))
                    containerDivBuilder.AddCssClass("invalid");
                else
                    containerDivBuilder.AddCssClass("valid");

            containerDivBuilder.InnerHtml += iconBuilder.ToString(TagRenderMode.Normal);
            containerDivBuilder.InnerHtml += btnCloseBuilder.ToString(TagRenderMode.Normal);
            containerDivBuilder.InnerHtml += helper.ValidationMessage(propertyName).ToString();

            return MvcHtmlString.Create(containerDivBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString CustomValidationSummary(this HtmlHelper helper, string validationMessage = "")
        {
            string retVal = "";
            if (helper.ViewData.ModelState.IsValid)
                return MvcHtmlString.Create(string.Empty);
            retVal += "<div class='alert custom-validation alert-danger alert-dismissable'><i class='fa fa-ban'></i><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><span>";
            if (!String.IsNullOrEmpty(validationMessage))
                retVal += helper.Encode(validationMessage);
            retVal += "</span>";
            foreach (var key in helper.ViewData.ModelState.Keys)
            {
                foreach (var err in helper.ViewData.ModelState[key].Errors)
                    retVal += "<p>" + MvcHtmlString.Create(helper.Encode(err.ErrorMessage)) + "</p>";
            }
            retVal += "</div>";
            return MvcHtmlString.Create(retVal.ToString());
        }
    }
}