﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace PisCRM.Hubs
{
    public class NotificationHub : Hub
    {
        public void Notify(string message, bool error = false)
        {
            Clients.Caller.notify(message, error);
        }
        public void Progress(int now, int max)
        {
            Clients.Caller.progress(now, max);
        }
    }
}