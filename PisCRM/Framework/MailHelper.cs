﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using PisCRM.Elmah;

namespace PisCRM.Framework
{
    public class MailHelper
    {
        //Calling the function from code
        //MailHelper.SendMailMessage("fromAddress@yourdomain.com", "toAddress@yourdomain.com", "bccAddress@yourdomain.com", 
        //"ccAddress@yourdomain.com", "Sample Subject", "Sample body of text for mail message")
        //
        //        Web.config
        //<?xml version="1.0"?>
        //<configuration>
        //   <system.net>
        //      <mailSettings>
        //         <smtp from="defaultEmail@yourdomain.com">
        //            <network host="smtp.yourdomain.com" port="25" userName="yourUserName" password="yourPassword"/>
        //         </smtp>
        //      </mailSettings>
        //   </system.net>
        //</configuration>
        //
        /// <summary>
        /// Sends an mail message
        /// </summary>
        /// <param name="from">Sender address</param>
        /// <param name="to">Recepient address</param>
        /// <param name="bcc">Bcc recepient</param>
        /// <param name="cc">Cc recepient</param>
        /// <param name="subject">Subject of mail message</param>
        /// <param name="body">Body of mail message</param>
        /// 
        public static bool SendMailMessage(string from, string to, string bcc, string cc, string subject, string body, bool isBodyHtml = false, MailAttachment[] attachments = null)
        {
            string smtpHost = ConfigurationManager.AppSettings.Get("SMTPServerAddress");
            string smtpPort = ConfigurationManager.AppSettings.Get("SMTPPort");
            string smtpUsername = ConfigurationManager.AppSettings.Get("SMTPUsername");
            string smtpPassword = ConfigurationManager.AppSettings.Get("SMTPPassword");
            bool smtpEnableSSL = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("SMTPEnableSSL"));

            //// Instantiate a new instance of MailMessage
            //MailMessage mMailMessage = new MailMessage();
            //// Set the sender address of the mail message
            //mMailMessage.From = new MailAddress(from);
            //// Set the recepient address of the mail message
            //mMailMessage.To.Add(new MailAddress(to));

            //// Check if the bcc value is null or an empty string
            //if ((bcc != null) && (bcc != string.Empty))
            //{
            //    // Set the Bcc address of the mail message
            //    mMailMessage.Bcc.Add(new MailAddress(bcc));
            //}
            //// Check if the cc value is null or an empty value
            //if ((cc != null) && (cc != string.Empty))
            //{
            //    // Set the CC address of the mail message
            //    mMailMessage.CC.Add(new MailAddress(cc));
            //}       // Set the subject of the mail message
            //mMailMessage.Subject = subject;
            //// Set the body of the mail message
            //mMailMessage.Body = body;

            //// Set the format of the mail message body as HTML
            //mMailMessage.IsBodyHtml = true;
            //mMailMessage.BodyEncoding = Encoding.GetEncoding("utf-8");
            //// Set the priority of the mail message to normal
            //mMailMessage.Priority = MailPriority.Normal;

            //// Instantiate a new instance of SmtpClient
            //SmtpClient mSmtpClient = new SmtpClient(smtpHost);
            //mSmtpClient.Timeout = 10000;
            //mSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            //mSmtpClient.UseDefaultCredentials = false;
            //mSmtpClient.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
            //mSmtpClient.Port = Convert.ToInt32(smtpPort);
            //mSmtpClient.EnableSsl = smtpEnableSSL;
            //// Send the mail message
            //mSmtpClient.Send(mMailMessage);






            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(smtpHost);

                mail.From = new MailAddress(smtpUsername);
                mail.To.Add(to);
                mail.Subject = subject;
                mail.SubjectEncoding = Encoding.UTF8;
                mail.Priority = MailPriority.Normal;

                if (attachments != null && attachments.Any())
                {
                    foreach (MailAttachment ma in attachments)
                    {
                        mail.Attachments.Add(ma.File);
                    }
                }

                mail.IsBodyHtml = isBodyHtml;
                mail.Body = body;

                SmtpServer.Port = Convert.ToInt32(smtpPort);
                SmtpServer.ServicePoint.MaxIdleTime = 1;
                SmtpServer.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
                SmtpServer.EnableSsl = smtpEnableSSL;

                SmtpServer.Send(mail);

                mail.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.LogError(ex, "Can not send email to address: " + to);
                return false;
            }
        }
    }
}