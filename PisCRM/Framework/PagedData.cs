﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PisCRM.Framework
{
    public class PagedData<T> where T : class
    {
        private int _pageSize;
        public virtual int PageSize
        {
            get
            {
                if (_pageSize == 0)
                    _pageSize = 20;
                return _pageSize;
            }

            set { _pageSize = value; }
        }
        public virtual int ItemCount { get; set; }
        public int StartItem
        {
            get
            {
                return 1 + (CurrentPage - 1) * PageSize;
            }
        }

        public int EndItem
        {
            get
            {
                if (CurrentPage == PageCount)
                {
                    return ItemCount;
                }
                else
                {
                    return CurrentPage * PageSize;
                }
            }
        }

        public int PageCount
        {
            get
            {
                if (ItemCount == 0)
                    return 1;

                return (int)Math.Ceiling((double)ItemCount / (double)PageSize);
            }
        }

        public int CurrentPage
        {
            get
            {
                var page = HttpContext.Current.Request.QueryString["page"];

                if (page == null)
                {
                    return 1;
                }

                int result = 0;

                if (!int.TryParse(page, out result))
                {
                    return 1;
                }

                return Math.Min(result, PageCount);
            }
        }

        public string SearchWord
        {
            get
            {
                var searchWord = HttpContext.Current.Request.QueryString["searchWord"];

                if (string.IsNullOrEmpty(searchWord))
                {
                    return string.Empty;
                }

                return searchWord;
            }
        }

        public IQueryable<T> Data { get; set; }

        public IQueryable<T> PreparePage<T>(IQueryable<T> entities)
        {
            ItemCount = entities.Count();

            if (CurrentPage == 1)
            {
                return entities.Take(PageSize);
            }

            var skip = (CurrentPage - 1) * PageSize;

            return entities.Skip(skip).Take(PageSize);
        }

        public IEnumerable<T> PreparePage<TT>(IEnumerable<T> entities)
        {
            ItemCount = entities.Count();

            if (CurrentPage == 1)
            {
                return entities.Take(PageSize);
            }

            var skip = (CurrentPage - 1) * PageSize;

            return entities.Skip(skip).Take(PageSize);
        }
    }
}