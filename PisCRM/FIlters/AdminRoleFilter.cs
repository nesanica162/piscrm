﻿using PisCRM.Contracts.Enums;
using PisCRM.Infrastructure;
using System.Web;
using System.Web.Mvc;

namespace PisCRM.FIlters
{
    public class AdminRoleFilter : ActionFilterAttribute
    {
        public ICurrentUser CurrentUser { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("/Account/Login");
                return;
            }
            else
            {
                var currentUserRoleId = CurrentUser.User.Role.RoleId;

                if (currentUserRoleId != (int)Roles.Admin)
                {
                    filterContext.Result = new RedirectResult("~/Redirect/");
                    return;
                }
            }
        }
    }
}