﻿using PisCRM.Repository;
using System.Collections.Generic;
using System.Linq;
using PisCRM.Contracts.IdentityModels;

namespace Domain.Managers
{
    public class RoleManager
    {
        private readonly ApplicationDbContext _context;

        public RoleManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<CustomRole> GetRoles()
        {
            return _context.Roles.ToList();
        }
    }
}
