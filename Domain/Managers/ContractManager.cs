﻿using PisCRM.Contracts;
using PisCRM.Repository;
using System.Linq;

namespace Domain.Managers
{
    public class ContractManager : BaseManager<Contract>
    {
        public ContractManager(DomainRepository<Contract> repository) : base(repository)
        {
        }

        public IQueryable<Contract> GetContracts(string searchWord)
        {
            IQueryable<Contract> query = Get();

            if (!string.IsNullOrEmpty(searchWord))
                query = query.Where(u => u.Name.Contains(searchWord));

            return query.OrderByDescending(u => u.DateCreated);
        }
    }
}
