﻿using PisCRM.Contracts;
using PisCRM.Repository;
using System.Linq;

namespace Domain.Managers
{
    public class ProductManager : BaseManager<Product>
    {
        public ProductManager(DomainRepository<Product> repository) : base(repository)
        {
        }

        public IQueryable<Product> GetProducts(string searchWord)
        {
            IQueryable<Product> query = Get();

            if (!string.IsNullOrEmpty(searchWord))
                query = query.Where(u => u.Name.Contains(searchWord));

            return query.OrderByDescending(u => u.DateCreated);
        }
    }
}
