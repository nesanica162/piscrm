﻿using PisCRM.Contracts;
using PisCRM.Repository;
using System.Linq;

namespace Domain.Managers
{
    public class ClientManager : BaseManager<Client>
    {
        public ClientManager(DomainRepository<Client> repository) : base(repository)
        {
        }

        public IQueryable<Client> GetClients(string searchWord)
        {
            IQueryable<Client> query = Get();

            if (!string.IsNullOrEmpty(searchWord))
                query = query.Where(u => u.Name.Contains(searchWord) || u.Email.Contains(searchWord));

            return query.OrderByDescending(u => u.DateCreated);
        }
    }
}
