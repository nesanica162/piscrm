﻿using Contracts.Dto;
using Framework;
using PisCRM.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Domain.Managers
{
    public class FileManager
    {
        public string GetFileUploadPath(int userId, int fileUploadDedication, int fileUploadEntity)
        {
            return Path.Combine(HttpContext.Current.Server.MapPath
                (string.Format("~/UserContent/{0}/{1}/{2}",
                    ((FileUploadEntity)fileUploadEntity).GetDescription(),
                    userId,
                    ((FileUploadDedication)fileUploadDedication).GetDescription())));
        }

        public List<FileUploadResult> Upload(int fileUploadEntity, int fileUploadDedication, int? userId, int? requestOfferId, List<HttpPostedFileBase> filesObj)
        {
            List<FileUploadResult> result = new List<FileUploadResult>();

            if (filesObj != null)
            {
                for (int i = 0; i < filesObj.Count(); i++)
                {
                    var file = filesObj[i];
                    if (file != null && file.ContentLength > 0)
                    {
                        try
                        {
                            string initialUserFilesPath = GetFileUploadPath(userId.Value, fileUploadDedication, fileUploadEntity);

                            if (!Directory.Exists(initialUserFilesPath))
                            {
                                Directory.CreateDirectory(initialUserFilesPath);
                            }

                            Guid fileId = Guid.NewGuid();
                            string path =
                                Path.Combine(initialUserFilesPath,
                                string.Format("{0}{1}", fileId, Path.GetExtension(file.FileName)));
                            file.SaveAs(path);


                            result.Add(new FileUploadResult()
                            {
                                FileId = fileId,
                                FileName = file.FileName,
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            return result;
        }
    }
}