﻿using Contracts.Dto;
using PisCRM.Contracts;
using PisCRM.Contracts.Enums;
using PisCRM.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Framework;
using Microsoft.AspNet.Identity;

namespace Domain.Managers
{
    public class EmployeeManager : BaseManager<ApplicationUser>
    {
        private readonly ApplicationUserManager _userManager;
        private readonly FileManager _fileManager;
        private readonly ApplicationDbContext _context;

        public EmployeeManager(DomainRepository<ApplicationUser> repository, ApplicationUserManager userManager,
            FileManager fileManager, ApplicationDbContext context) : base(repository)
        {
            _userManager = userManager;
            _fileManager = fileManager;
            _context = context;
        }

        public IQueryable<ApplicationUser> GetEmployees(string searchWord)
        {
            IQueryable<ApplicationUser> query = Get();

            if (HttpContext.Current.User.IsInRole((Roles.Employee).GetDescription()))
                query = query.Where(u => u.Roles.FirstOrDefault().RoleId == (int)Roles.Employee);

            if (!string.IsNullOrEmpty(searchWord))
                query = query.Where(u => u.FirstName.Contains(searchWord) || u.LastName.Contains(searchWord)
                    || u.Email.Contains(searchWord)
                    || (u.FirstName + u.LastName).Replace(" ", string.Empty).Contains(searchWord.Replace(" ", string.Empty)));

            return query.OrderByDescending(u => u.DateCreated);
        }

        public FileUploadResult SaveProfilePhoto(int userId, HttpPostedFileBase file)
        {
            List<HttpPostedFileBase> files = new List<HttpPostedFileBase>();

            if (file != null)
            {
                files.Add(file);

                List<FileUploadResult> uploadProfilePictureResult
                    = _fileManager.Upload((int)FileUploadEntity.User,
                    (int)FileUploadDedication.UserProfilePhoto, userId, null, files);

                if (uploadProfilePictureResult != null && uploadProfilePictureResult.Count == 1)
                {
                    var profilePhotoUploadResult = uploadProfilePictureResult[0];

                    var user = GetSingle(userId);

                    if(user != null)
                    {
                        user.PhotoFileId = profilePhotoUploadResult.FileId;
                        user.PhotoFileName = profilePhotoUploadResult.FileName;
                        Update(user);
                    }

                    return profilePhotoUploadResult;
                }
            }

            return null;
        }

        public bool SaveEmployeeRole(int userId, int roleId)
        {
            var user = GetSingle(userId);
            if (user?.Role != null)
            {
                var userRole = _context.AspNetUserRoles.FirstOrDefault(u => u.UserId == userId);

                if (userRole != null)
                {
                    if (userRole.RoleId != roleId)
                    {
                        _context.AspNetUserRoles.Remove(userRole);

                        var result = _userManager.AddToRole(user.Id, ((Roles)roleId).GetDescription());
                        return result.Succeeded;
                    }
                }
            }

            return true;
        }
    }
    //public class EmployeeManager : UserManager<ApplicationUser, int>, IEmployeeManager
    //{
    //    public EmployeeManager(IUserStore<ApplicationUser, int> store) : base(store)
    //    {
    //    }
    //}
}
