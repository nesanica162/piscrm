﻿using Contracts.Managers;
using PisCRM.Contracts.Interfaces;
using PisCRM.Repository;
using System.Linq;

namespace Domain
{
    public class BaseManager<T> : IManager<T> where T : class, IEntity, new()
    {
        private readonly IDomainRepository<T> Repository;

        public BaseManager() { }

        protected BaseManager(IDomainRepository<T> repository)
        {
            Repository = repository;
        }

        public T GetSingle(int id)
        {
            return Repository.GetSingle(id);
        }

        public IQueryable<T> Get()
        {
            return Repository.Get();
        }

        public void Insert(T entity, bool persistToDB = true)
        {
            Repository.Insert(entity);
        }

        public void Update(T entity, bool persistToDB = true)
        {
            Repository.Update(entity);
        }

        public void Delete(int id)
        {
            Repository.Delete(id);
        }
    }
}
