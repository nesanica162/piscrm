﻿using PisCRM.Contracts.Interfaces;
using System.Data.Entity;
using System.Linq;

namespace PisCRM.Repository
{
    public class DomainRepository<T> : IDomainRepository<T> where T : class, IEntity, new()
    {
        private readonly ApplicationDbContext _context;

        public DomainRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        
        #region GetSingle

        public virtual T GetSingle(int id)
        {
            return GetSingle(id, _context);
        }
        public virtual T GetSingle(int id, ApplicationDbContext context)
        {
            return Get(context).FirstOrDefault(e => e.Id == id);
        }

        #endregion

        #region Get

        public virtual IQueryable<T> Get()
        {
            return Get(_context);
        }
        public virtual IQueryable<T> Get(ApplicationDbContext context)
        {
            DbSet<T> entities = context.Set<T>();

            return FilterEntities(entities);
        }

        #endregion

        #region Insert

        public virtual void Insert(T entity, bool persistToDB = true)
        {
            Insert(entity, _context, persistToDB);
        }
        public virtual void Insert(T entity, ApplicationDbContext context, bool persistToDB = true)
        {
            DbSet<T> entities = context.Set<T>();
            entities.Add(entity);

            if (persistToDB)
            {
                context.SaveChanges();
            }
        }

        #endregion

        #region Update

        public virtual void Update(T entity, bool persistToDB = true)
        {
            Update(entity, _context, persistToDB);
        }
        public virtual void Update(T entity, ApplicationDbContext context, bool persistToDB = true)
        {
            if (persistToDB)
                context.SaveChanges();
        }

        #endregion

        #region Delete

        public virtual void Delete(int id)
        {
            Delete(id, _context);
        }
        public virtual void Delete(int id, ApplicationDbContext context)
        {
            DbSet<T> entities = context.Set<T>();

            var entity = GetSingle(id, context);
            entities.Remove(entity);

            context.SaveChanges();
        }

        #endregion

        #region Filter

        private IQueryable<T> FilterEntities(DbSet<T> entities)
        {
            var filteredEntities = entities.AsQueryable();

            return FilterEntities(filteredEntities);
        }
        internal IQueryable<T> FilterEntities(IQueryable<T> filteredEntities)
        {
            return filteredEntities;
        }

        #endregion
    }
}