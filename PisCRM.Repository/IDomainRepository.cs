﻿using System.Linq;
using PisCRM.Contracts.Interfaces;

namespace PisCRM.Repository
{
    public interface IDomainRepository<T> where T : class, IEntity, new()
    {
        T GetSingle(int id);
        T GetSingle(int id, ApplicationDbContext context);
        IQueryable<T> Get();
        IQueryable<T> Get(ApplicationDbContext context);
        void Insert(T entity, bool persistToDB = true);
        void Insert(T entity, ApplicationDbContext context, bool persistToDB = true);
        void Update(T entity, bool persistToDB = true);
        void Update(T entity, ApplicationDbContext context, bool persistToDB = true);
        void Delete(int id);
        void Delete(int id, ApplicationDbContext context);
    }
}
