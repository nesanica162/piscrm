﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using PisCRM.Contracts.Interfaces;
using PisCRM.Contracts.IdentityModels;
using Contracts.Dto;

namespace PisCRM.Contracts
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser<int, CustomUserLogin, CustomUserRole, CustomUserClaim>, IEntity
    {
        public DateTime DateCreated { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string WorkPosition { get; set; }
        public Guid? PhotoFileId { get; set; }
        public string PhotoFileName { get; set; }

        public string FullName
        {
            get
            {
                string userFullName = string.Empty;

                userFullName = string.Format("{0} {1}", this.FirstName, this.LastName);

                if (string.IsNullOrEmpty(userFullName))
                {
                    userFullName = this.Email;
                }

                return userFullName;
            }
        }

        public CustomUserRole Role
        {
            get
            {
                return this.Roles.FirstOrDefault();
            }
        }

        public string ProfilePictureUrl
        {
            get
            {
                string profilePictureUrl = string.Empty;
                if (this.PhotoFileId.HasValue && !string.IsNullOrEmpty(this.PhotoFileName))
                {
                    profilePictureUrl =
                        ConfigurationSettings.GenerateStaticUrl(string.Format("/UserContent/User/{0}/ProfileImage/{1}{2}",
                        this.Id, this.PhotoFileId, new FileInfo(this.PhotoFileName).Extension));

                    return profilePictureUrl;
                }

                if (string.IsNullOrEmpty(profilePictureUrl))
                    profilePictureUrl = ConfigurationSettings.GenerateStaticUrl("/Content/images/default_profile.jpg");

                return profilePictureUrl;
            }
        }

        public ClaimsIdentity GenerateUserIdentity(UserManager<ApplicationUser, int> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}