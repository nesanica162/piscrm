﻿using PisCRM.Contracts.Interfaces;
using System.Linq;

namespace Contracts.Managers
{
    public interface IManager<T> where T : class, IEntity, new()
    {
        T GetSingle(int id);
        IQueryable<T> Get();
        void Insert(T entity, bool persistToDB = true);
        void Update(T entity, bool persistToDB = true);
        void Delete(int id);
    }
}
