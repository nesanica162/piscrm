﻿using PisCRM.Contracts.Interfaces;
using System;

namespace PisCRM.Contracts
{
    public class ClientFile : IEntity
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set ; }
        public Guid FileId { get; set; }
        public string FileName { get; set; }
        public virtual Client Client { get; set; }
        public int ClientId { get; set; }
    }
}