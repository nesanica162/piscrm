﻿using PisCRM.Contracts.Interfaces;
using System;
using System.Collections.Generic;

namespace PisCRM.Contracts
{
    public class Client : IEntity
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set ; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public string Projects { get; set; }
        public virtual List<ClientFile> ClientFiles { get; set; }
        public virtual List<Contract> Contracts { get; set; }
    }
}