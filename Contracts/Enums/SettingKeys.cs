﻿using System.ComponentModel;

namespace PisCRM.Contracts.Enums
{
    public enum Roles
    {
        [Description("Администратор")]
        Admin = 1,

        [Description("Вработен")]
        Employee = 2,
    }

    public enum FileUploadEntity
    {
        [Description("User")]
        User = 0
    }

    public enum FileUploadDedication
    {
        [Description("ProfileImage")]
        UserProfilePhoto = 0
    }
}