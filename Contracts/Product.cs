﻿using PisCRM.Contracts.Interfaces;
using System;
using System.Collections.Generic;

namespace PisCRM.Contracts
{
    public class Product : IEntity
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<ProductFile> ProductFiles { get; set; }
    }
}