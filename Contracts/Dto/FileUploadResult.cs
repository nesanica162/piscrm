﻿using System;

namespace Contracts.Dto
{
    public class FileUploadResult
    {
        public Guid FileId { get; set; }
        public string FileName { get; set; }
    }
}
