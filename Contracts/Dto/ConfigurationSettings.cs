﻿using System;
using System.Configuration;
using System.Web;

namespace Contracts.Dto
{
    public static class ConfigurationSettings
    {
        public static string AppDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["AppDomain"].ToString();
            }
        }
        public static string FilesDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["FilesDomain"].ToString();
            }
        }
        public static string CDN1
        {
            get
            {
                return ConfigurationManager.AppSettings["CDN1"].ToString();
            }
        }
        public static string CDN2
        {
            get
            {
                return ConfigurationManager.AppSettings["CDN2"].ToString();
            }
        }
        public static string CDN3
        {
            get
            {
                return ConfigurationManager.AppSettings["CDN3"].ToString();
            }
        }

        public static string SMTPUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPUsername"].ToString();
            }
        }

        public static string GetNextCDN(int counterCDN = 0)
        {
            string result = String.Empty;

            counterCDN++;

            if (counterCDN % 3 == 0)
            {
                result = CDN1;
            }

            if (String.IsNullOrEmpty(result) && counterCDN % 2 == 0)
            {
                result = CDN2;
            }

            if (String.IsNullOrEmpty(result))
            {
                result = CDN3;
            }

            return result;
        }

        public static string GenerateStaticUrl(string url)
        {
            if (url.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
            {
                return url;
            }

            var cdn = GetNextCDN();

            if (!String.IsNullOrEmpty(cdn))
            {
                if (HttpContext.Current.Request.IsSecureConnection)
                {
                    url = String.Format("https://{0}{1}", cdn, url);
                }
                else
                {
                    url = String.Format("http://{0}{1}", cdn, url);
                }
            }
            else
            {
                url = String.Format("http://{0}{1}", ConfigurationSettings.FilesDomain, url);
            }

            return url;
        }
    }
}