﻿using System;

namespace PisCRM.Contracts.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
        DateTime DateCreated { get; set; }
    }
}
