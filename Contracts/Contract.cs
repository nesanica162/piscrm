﻿using PisCRM.Contracts.Interfaces;
using System;

namespace PisCRM.Contracts
{
    public class Contract : IEntity
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual Client Client { get; set; }
        public int ClientId { get; set; }
    }
}