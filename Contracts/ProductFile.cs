﻿using PisCRM.Contracts.Interfaces;
using System;

namespace PisCRM.Contracts
{
    public class ProductFile : IEntity
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public Guid FileId { get; set; }
        public string FileName { get; set; }
        public virtual Product Product { get; set; }
        public int ProductId { get; set; }
    }
}